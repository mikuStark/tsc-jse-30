package ru.tsc.karbainova.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.dto.Domain;
import ru.tsc.karbainova.tm.enumerated.Role;

import java.io.FileOutputStream;

public class DataJsonSaveFasterXmlCommand extends AbstractDataCommand {
    @Override
    public String name() {
        return "json-faster-save";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Json faster save";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NonNull final Domain domain = getDomain();
        @NonNull final ObjectMapper objectMapper = new ObjectMapper();
        @NonNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NonNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_FASTERXML_JSON);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    public @Nullable Role[] roles() {
        return new Role[0];
    }
}
