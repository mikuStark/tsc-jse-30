package ru.tsc.karbainova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import ru.tsc.karbainova.tm.api.entity.IWBS;

@NoArgsConstructor
public abstract class AbstractOwnerEntity extends AbstractEntity implements IWBS {

    @Getter
    @Setter
    @NonNull
    private String userId;

}
