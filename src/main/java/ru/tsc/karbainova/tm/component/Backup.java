package ru.tsc.karbainova.tm.component;

import lombok.NonNull;
import lombok.SneakyThrows;

import static ru.tsc.karbainova.tm.command.data.BackupLoadCommand.BACKUP_LOAD;
import static ru.tsc.karbainova.tm.command.data.BackupSaveCommand.BACKUP_SAVE;

public class Backup extends Thread {

    @NonNull
    final Bootstrap bootstrap;

    public Backup(@NonNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    @NonNull
    private static final int interval = 30000;

    @Override
    @SneakyThrows
    public void run() {
        while (true) {
            save();
            Thread.sleep(interval);
        }
    }

    private void save() {
        bootstrap.executeCommand(BACKUP_SAVE);
    }

    public void init() {
        load();
        start();
    }

    private void load() {
        bootstrap.executeCommand(BACKUP_LOAD);
    }
}
